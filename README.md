# danfe-press

Impressão térmica utilizando driver 'cups'.

![](../header.png)

## Instalação

 Linux:

```sh
cd danfe-press/escpos-php
composer install
```
[Guia de instalação no Centos 7](https://phoenixnap.com/kb/how-to-install-and-use-php-composer-on-centos-7)

## Requisitos

IMPRESSORA MODELO BEMATECH MP 4200 TH

php7 (compilar codigo fonte)
inotify (monitorar ações do diretorio)
sudo apt-get install php-gd (Gerar Imagem do QR Code)

```sh
yum install php70 php70-php-pear php70-php-devel php70-php-gd
ln -sf /opt/remi/php70/root/usr/bin/pear /usr/bin/pear
ln -sf /opt/remi/php70/root/usr/bin/pecl /usr/bin/pecl
ln -sf /opt/remi/php70/ /usr/bin/php
ln -sf /opt/remi/php70/root/usr/bin/phpize /usr/bin/phpize
php70 -i | grep "Loaded Configuration File"
ln -sf /usr/bin/php70 /usr/bin/php
php -v
pecl install inotify
php -r "var_dump(function_exists('inotify_init'));"
```
colocar a entrada extension="inotify.so" no php.ini

Em servidor oracle os caminhos mudam:
```sh
ln -sf /opt/rh/rh-php70/root/bin/pear /usr/bin/pear
ln -sf /opt/rh/rh-php70/root/bin/pecl /usr/bin/pecl
ln -sf /opt/rh/rh-php70/root/bin/php /usr/bin/php
ln -sf /opt/rh/rh-php70/root/bin/phpize /usr/bin/phpize
php -i | grep "Loaded Configuration File"
```


## Exemplo de uso
Executar processo exibe-danfe
```sh
~/danfe-press/escpos-php/example/interface/exibe-danfe.php
```
O qual ficará esperando uma alteração na pasta (utilizando inotify)
```sh
~/danfe-press/escpos-php/example/interface/teste-xml
```
Caso algum arquivo XML seja copiado para tal diretorio, será enviado uma requisição para impressora térmica com a formatação correta dos dados convertidos. Seguindo o padrão CUPS.


## Meta

Nicolas Aoki – nick_aoki@hotmail.com

Projetos open-source utilizados:

[NFePHP](https://github.com/nfephp-org/nfephp)
[posprint](https://github.com/nfephp-org/posprint)
[escpos-php](https://github.com/mike42/escpos-php)
